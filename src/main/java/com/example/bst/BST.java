package com.example.bst;

/**
 * Implementation of BST (Binary Search Tree) without duplicated values
 */
public class BST<T extends Comparable<T>> {
    private Node root;

    /**
     * O(log n); Omega(1)
     * Method adds object to BST by iteration; duplicates are not being added
     *
     * @param value an object to be added
     */
    public void insertIterative(T value) {
        Node node = new Node(value);
        if (root == null) {
            //BST is empty - add value as root
            root = node;
        } else {
            //if BST is not empty
            Node temp = root;
            while (true) {
                if (temp.value.compareTo(value) > 0) {
                    if (temp.leftChild == null) {
                        temp.leftChild = node;
                        return;
                    }
                    temp = temp.leftChild;
                } else if (temp.value.compareTo(value) < 0) {
                    if (temp.rightChild == null) {
                        temp.rightChild = node;
                        return;
                    }
                    temp = temp.rightChild;
                } else {
                    return;
                }
            }
        }
    }

    /**
     * Method calls private method <code>insertRec(Node parent, T value)</code> with object <code>root</code>
     * as first parameter
     *
     * @param value an object to be added
     */
    public void insertRecursive(T value) {
        if (root == null) {
            root = new Node(value);
        } else {
            insertRec(root, value);
        }
    }

    /**
     * O(log n); Omega(1)
     * Private method insert by recursion an object somewhere under specific parent
     *
     * @param parent node which is treated as root of sub-BST
     * @param value  an object to be added
     */
    private void insertRec(Node parent, T value) {
        if (parent.value.compareTo(value) > 0) {
            if (parent.leftChild == null) {
                parent.leftChild = new Node(value);
            } else {
                insertRec(parent.leftChild, value);
            }
        } else if (parent.value.compareTo(value) < 0) {
            if (parent.rightChild == null) {
                parent.rightChild = new Node(value);
            } else {
                insertRec(parent.rightChild, value);
            }
        }
    }

    /**
     * O(log n); Omega(1)
     * Method checks by iteration if BST contains specific object
     *
     * @param value the object to be checked
     * @return <code>true</code> when BST includes value, <code>false</code> otherwise
     */
    public boolean containsIterative(T value) {
        Node temp = root;
        while (temp != null) {
            if (temp.value.compareTo(value) == 0) {
                return true;
            }
            if (temp.value.compareTo(value) > 0) {
                temp = temp.leftChild;
            } else {
                temp = temp.rightChild;
            }
        }
        return false;
    }

    /**
     * Method calls private method <code>containsRec(Node parent, T value)</code> with object <code>root</code>
     * as first parameter
     *
     * @param value the object to be checked
     */
    public boolean containsRecursive(T value) {
        return containsRec(root, value);
    }

    /**
     * O(log n); Omega(1)
     * Private method checks by recursion if BST contains specific object
     *
     * @param parent node from which you would like to start checking
     * @param value  an object to be checked
     * @return <code>true</code> when BST includes value, <code>false</code> otherwise
     */
    private boolean containsRec(Node parent, T value) {
        if (parent == null) {
            return false;
        }
        if (parent.value.compareTo(value) == 0) {
            return true;
        }
        if (parent.value.compareTo(value) < 0) {
            return containsRec(parent.rightChild, value);
        }
        if (parent.value.compareTo(value) > 0) {
            return containsRec(parent.leftChild, value);
        }
        return false;
    }

    /**
     * O(log n); Omega(1)
     * Method remove object from BST by iteration, if  object is not in BST - no action
     *
     * @param value an object to be removed
     */
    public void removeIterative(T value) {
        if (root == null) {
            return;
        }
        //find node which contains value we want to remove
        Node parent = root;
        Node grandFather = root;
        while (parent != null && parent.value.compareTo(value) != 0) {
            grandFather = parent;
            if (parent.value.compareTo(value) > 0) {
                parent = parent.leftChild;
            } else {
                parent = parent.rightChild;
            }
        }
        if (parent == null) {
            //exit - object not found in BST
            return;
        }
        if (parent.leftChild == null && parent.rightChild == null) {
            //node found (parent) has no children - it is a leaf; we need to remove this node
            if (root.value == value) {
                //if value we want to remove is a root
                root = null;
                return;
            }
            if (grandFather.value.compareTo(parent.value) > 0) {
                grandFather.leftChild = null;
            } else {
                grandFather.rightChild = null;
            }
        } else if (parent.leftChild != null && parent.rightChild == null) {
            //node found (parent) has just left child; we need to connect grand-father directly with left child
            if (root.value == value) {
                //if value we want to remove is a root
                root = parent.leftChild;
                return;
            }
            if (grandFather.value.compareTo(parent.value) > 0) {
                grandFather.leftChild = parent.leftChild;
            } else {
                grandFather.rightChild = parent.leftChild;
            }
        } else if (parent.leftChild == null) {
            //node found (parent) has just right child;  we need to connect grand-father directly with right child
            if (root.value == value) {
                //if value we want to remove is a root
                root = parent.rightChild;
                return;
            }
            if (grandFather.value.compareTo(parent.value) > 0) {
                grandFather.leftChild = parent.rightChild;
            } else {
                grandFather.rightChild = parent.rightChild;
            }
        } else {
            //node found (parent) has two children - we need to find minimum leaf under right child
            Node newLeft = parent.leftChild;
            Node newRight = parent.rightChild;
            if (parent.rightChild.leftChild == null) {
                //if left grand son is null, then min object is right son
                if (root.value == value) {
                    //if value we want to remove is a root
                    root = newRight;
                    root.leftChild = newLeft;
                } else if (grandFather.value.compareTo(parent.value) < 0) {
                    grandFather.rightChild = parent.rightChild;
                    grandFather.rightChild.leftChild = newLeft;
                } else {
                    grandFather.leftChild = parent.rightChild;
                    grandFather.leftChild.leftChild = newLeft;
                }
            } else {
                //find lowest value under right grand son and replace parent by found object
                Node lowest = findLowestSuccessorOf(parent);
                if (grandFather.value.compareTo(parent.value) < 0) {
                    removeIterative(lowest.value);
                    if (root.value == value) {
                        //if value we want to remove is a root
                        root = lowest;
                        grandFather = root;
                    } else {
                        grandFather.rightChild = lowest;
                    }
                    grandFather.rightChild.rightChild = newRight;
                    grandFather.rightChild.leftChild = newLeft;
                } else {
                    removeIterative(lowest.value);
                    if (root.value == value) {
                        //if value we want to remove is a root
                        root = lowest;
                        root.rightChild = newRight;
                        root.leftChild = newLeft;
                    } else {
                        grandFather.leftChild = lowest;
                        grandFather.leftChild.leftChild = newLeft;
                    }
                }
            }
        }
    }

    /**
     * method returns min object under right child, this is private method which will be used in
     * <code>removeRec(Node parent, T value)</code> and <code>removeIterative(T value)</code>;
     * conditions under mentioned method allow us to assume, that in below method <code>parent</code> always
     * has not null right child
     */
    private Node findLowestSuccessorOf(Node parent) {
        Node temp = parent.rightChild;
        while (temp.leftChild != null) {
            temp = temp.leftChild;
        }
        return temp;
    }

    /**
     * Method calls private method <code>removeRec(Node parent, T value)</code> with object <code>root</code>
     * as first parameter
     *
     * @param value an object to be removed
     */
    public void removeRecursive(T value) {
        root = removeRec(root, value);
    }

    /**
     * O(log n); Omega(1)
     * Method remove object from BST by recursion, if  object is not in BST - no action
     *
     * @param parent is root of BST (it can be sub-BST of original BST)
     * @param value  an object to be removed
     */
    private Node removeRec(Node parent, T value) {
        if (parent == null) {
            /*at very beginning this step means that root is null (so BST is empty) or during recursion
             we looked over all objects of BST and object we want to remove has been not added to BST earlier*/
            return null;
        }
        if (parent.value.compareTo(value) == 0) {
            //cases when object we want to remove is equal to parent's value
            if (parent.leftChild == null && parent.rightChild == null) {
                //parent has no children - it is a leaf; we need to remove this parent
                return null;
            }
            if (parent.rightChild == null) {
                //parent has just left child; we need to connect grand-father directly with left child
                return parent.leftChild;
            }
            if (parent.leftChild == null) {
                //parent has just right child;  we need to connect grand-father directly with right child
                return parent.rightChild;
            }
            //parent has two children - we need to find minimum leaf under right child
            Node tempLeft = parent.leftChild;
            Node tempRight = parent.rightChild;

            if (parent.rightChild.leftChild == null) {
                //if left grand son is null, then min object is right son
                parent = parent.rightChild;
            } else {
                //find lowest value under right grand son and replace parent by found object
                parent = findLowestSuccessorOf(parent);
            }
            parent.rightChild = removeRec(tempRight, parent.value);
            parent.leftChild = tempLeft;
            return parent;
        } else {
            //when object we want to remove is not equal to parent's value, we level by level go down BST
            if (parent.value.compareTo(value) > 0) {
                parent.leftChild = removeRec(parent.leftChild, value);
            } else {
                parent.rightChild = removeRec(parent.rightChild, value);
            }
        }
        return parent;
    }

    /**
     * method print BST as in-order beginning from its root
     */
    public void displayInOrder() {
        display(root);
    }

    /**
     * method print BST as in-order
     */
    private void display(Node root) {
        if (root != null) {
            display(root.leftChild);
            System.out.print(root.value + "  ");
            display(root.rightChild);
        }
    }

    private class Node {
        private final T value;
        private Node leftChild;
        private Node rightChild;

        private Node(T value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "value=" + value;
        }
    }
}
