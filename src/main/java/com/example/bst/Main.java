package com.example.bst;

import com.example.bst.elements.Person;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Press 1: manage you own BST\n" +
                "Press 2: EXIT");
        String wrongInput = "Incorrect input. Try again. ";
        Scanner scanner = new Scanner(System.in);
        int option = 0;
        boolean flag = true;
        while (flag) {
            try {
                option = scanner.nextInt();
                flag = false;
                if (option != 1 && option != 2) {
                    System.out.println(wrongInput);
                    flag = true;
                }
            } catch (InputMismatchException e) {
                System.out.println(wrongInput);
                scanner.nextLine();
            }
        }
        if (option == 1) {
            manageBST();
        } else {
            System.out.println("Bye bye!!!");
            System.exit(1);
        }
    }

    /**
     * Get from user option number, depending of choice, next method runs. Possible values:
     * 1) create BST of Integers
     * 2) create BST of People
     * 3) exit program
     */
    private static void manageBST() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Press 1: create BST of Integers (iterative methods)\n" +
                "Press 2: create BST of People (recursive methods)\n" +
                "Press 3: EXIT");
        String wrongInput = "Incorrect input. Try again. ";
        int input = 0;
        boolean flag = true;
        while (flag) {
            try {
                input = scanner.nextInt();
                flag = false;
                if (input != 1 && input != 2 && input != 3) {
                    System.out.println(wrongInput);
                    flag = true;
                }
            } catch (InputMismatchException e) {
                System.out.println(wrongInput);
                scanner.nextLine();
            }
        }
        if (input == 1) {
            runIntegerBST(scanner);
        } else if (input == 2) {
            runPeopleBST(scanner);
        } else {
            System.out.println("Bye bye");
            System.exit(1);
        }
    }

    /**
     * Create BST of Integers, user can choose 4 options, depending on choice, next methods run
     * 1) insert number to BST
     * 2) check if BST contains number
     * 3) remove number from BST
     * 4) exit program
     */
    private static void runIntegerBST(Scanner scanner) {
        BST<Integer> integerBST = new BST<>();
        while (true) {
            int input = getOptionBST(scanner);
            if (input == 1) {
                insertIntToBST(scanner, integerBST);
            } else if (input == 2) {
                ifIntBSTContains(scanner, integerBST);
            } else if (input == 3) {
                removeIntFromBST(scanner, integerBST);
            } else {
                System.out.println("Bye, bye");
                System.exit(1);
            }
        }
    }

    /**
     * Insert number to BST and display updated BST in order. Number get from method <code>getNumberForAction</code>
     */
    private static void insertIntToBST(Scanner scanner, BST<Integer> bst) {
        String message = "Type in number you would like to add to BST (higher than 0)";
        String incorrect = "Incorrect input. Try again. ";
        int number = getNumberForAction(scanner, message, incorrect);
        boolean result = bst.containsIterative(number);
        if (result) {
            System.out.println("Duplicate cannot be added");
        }
        bst.insertIterative(number);
        displayBST(bst);
    }

    /**
     * Check if BST contains number and display BST in order. Number get from method <code>getNumberForAction</code
     */
    private static void ifIntBSTContains(Scanner scanner, BST<Integer> bst) {
        String message = "Type in number you would like to check (higher than 0)";
        String incorrect = "Incorrect input. Try again. ";
        int number = getNumberForAction(scanner, message, incorrect);
        boolean result = bst.containsIterative(number);
        String text = result ? "BST contains: " + number : "BST does not contain: " + number;
        System.out.println(text);
        displayBST(bst);
    }

    /**
     * Remove number from BST and display BST in order. Number get from method <code>getNumberForAction</code
     */
    private static void removeIntFromBST(Scanner scanner, BST<Integer> bst) {
        String message = "Type in number you would like to remove from BST (higher than 0)";
        String incorrect = "Incorrect input. Try again. ";
        int number = getNumberForAction(scanner, message, incorrect);
        bst.removeIterative(number);
        displayBST(bst);
    }

    /**
     * Create BST of Person, user can choose 4 options, depending on choice, next methods run
     * 1) insert person to BST
     * 2) check if BST contains person
     * 3) remove person from BST
     * 4) exit program
     */
    private static void runPeopleBST(Scanner scanner) {
        BST<Person> peopleBST = new BST<>();
        while (true) {
            int input = getOptionBST(scanner);
            if (input == 1) {
                insertPersonToBST(scanner, peopleBST);
            } else if (input == 2) {
                ifPeopleBSTContains(scanner, peopleBST);
            } else if (input == 3) {
                removePersonFromBST(scanner, peopleBST);
            } else {
                System.out.println("Bye, bye");
                System.exit(1);
            }
        }
    }

    /**
     * Insert person to BST and display updated BST in order. Person get from method <code>getPersonForAction</code>
     */
    private static void insertPersonToBST(Scanner scanner, BST<Person> bst) {
        System.out.println("Please provide details of person you would like to add to BST");
        Person person = getPersonForAction(scanner);
        boolean result = bst.containsRecursive(person);
        if (result) {
            System.out.println("Duplicate cannot be added");
        }
        bst.insertRecursive(person);
        displayBST(bst);
    }

    /**
     * Check if BST contains person and display BST in order. Person get from method <code>getPersonForAction</code
     */
    private static void ifPeopleBSTContains(Scanner scanner, BST<Person> bst) {
        System.out.println("Please provide details of person you would like to check");
        Person person = getPersonForAction(scanner);
        boolean result = bst.containsRecursive(person);
        String text = result ? "BST contains: " + person : "BST does not contain: " + person;
        System.out.println(text);
        displayBST(bst);
    }

    /**
     * Remove person from BST and display BST in order. Person get from method <code>getPersonForAction</code
     */
    private static void removePersonFromBST(Scanner scanner, BST<Person> bst) {
        System.out.println("Please provide details of person you would like to remove from BST");
        Person person = getPersonForAction(scanner);
        bst.removeRecursive(person);
        displayBST(bst);
    }

    /**
     * Get from user number to be used in insert, contains, remove methods
     *
     * @param message    - instruction displayed at very beginning of method
     * @param wrongInput - text displayed when incorrect data provided
     */
    private static int getNumberForAction(Scanner scanner, String message, String wrongInput) {
        boolean flag;
        System.out.println(message);
        int number = 0;
        flag = true;
        while (flag) {
            try {
                number = scanner.nextInt();
                flag = false;
                if (number <= 0) {
                    System.out.println(wrongInput);
                    flag = true;
                }
            } catch (InputMismatchException e) {
                System.out.println(wrongInput);
                scanner.nextLine();
            }
        }
        return number;
    }

    /**
     * Get from user person to be used in insert, contains, remove methods
     */
    private static Person getPersonForAction(Scanner scanner) {
        scanner.nextLine();
        System.out.println("Name: ");
        String name = scanner.nextLine();
        name = name.toLowerCase();
        System.out.println("Surname: ");
        String surname = scanner.nextLine();
        surname = surname.toLowerCase();
        return new Person(surname, name);
    }

    /**
     * Get from user option number for next steps, possible values:
     * 1) add to BST
     * 2) check if BST contains
     * 3) remove from BST
     * 4) EXIT
     */
    private static int getOptionBST(Scanner scanner) {
        System.out.println("Press 1: add to BST\n" +
                "Press 2: check if BST contains\n" +
                "Press 3: remove from BST\n" +
                "Press 4: EXIT");
        String wrongInput = "Incorrect input. Try again.";
        int input = 0;
        boolean flag = true;
        while (flag) {
            try {
                input = scanner.nextInt();
                flag = false;
                if (input != 1 && input != 2 && input != 3 && input != 4) {
                    System.out.println(wrongInput);
                    flag = true;
                }
            } catch (InputMismatchException e) {
                System.out.println(wrongInput);
                scanner.nextLine();
            }
        }
        return input;
    }

    /**
     * Dislay BST in order
     *
     * @param bst - BST to be dislayed
     */
    private static void displayBST(BST bst) {
        System.out.println("BST in order:");
        System.out.println("---------------------------------------------------");
        bst.displayInOrder();
        System.out.println("\n---------------------------------------------------");
    }
}



