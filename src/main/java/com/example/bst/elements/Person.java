package com.example.bst.elements;

public class Person implements Comparable<Person> {
    private final String name;
    private final String surname;

    public Person(String surname, String name) {
        this.name = name;
        this.surname = surname;
    }

    @Override
    public String toString() {
        return surname + "+" + name;
    }

    @Override
    public int compareTo(Person person) {
        return surname.equals(person.surname)
                ? name.compareTo(person.name)
                : surname.compareTo(person.surname);
    }
}



